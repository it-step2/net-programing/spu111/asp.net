﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MovieCatalog.Domain.Models;
using MovieCatalog.MVCView.Controllers;
using MovieCatalog.MVCView.ViewModels.Shared;

namespace MovieCatalog.MVCView.ViewModels;

public sealed class CategoriesViewModel : PaginatedFilteredViewModel
{
    private static readonly IReadOnlyCollection<string> ColumnNames = new[]
    {
        nameof(Category.Name)
    };
    
    private static readonly IReadOnlyCollection<SelectListItem> InitialSortColumns = new[]
    {
        new SelectListItem("", "Id", true),
        new SelectListItem("Name", "Name")
    };
    
    public override IEnumerable<string> Columns => ColumnNames;
    public override IEnumerable<SelectListItem> SortColumns => InitialSortColumns.ToList();

    public CategoriesViewModel() : base(typeof(Category), typeof(CategoriesController))
    {
    }
    
    public override string GetColumnValue(Model model, string column)
    {
        if (model is not Category category)
            throw new ArgumentException($"Model '{model.GetType()}' is not a category.");
        
        return column switch
        {
            nameof(Category.Id) => category.Id.ToString(),
            nameof(Category.Name) => category.Name,
            _ => throw new ArgumentException($"Column '{column}' is not supported.")
        };
    }
}