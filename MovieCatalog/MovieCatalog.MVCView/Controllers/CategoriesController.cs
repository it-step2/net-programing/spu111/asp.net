﻿using MovieCatalog.Application.Categories;
using MovieCatalog.Domain.Models;

namespace MovieCatalog.MVCView.Controllers;

public sealed class CategoriesController : FilteredPaginatedController<Category>
{
    private readonly ICategoryService _categoryService;

    public CategoriesController(ICategoryService categoryService) : base(categoryService)
    {
        _categoryService = categoryService;
    }
}