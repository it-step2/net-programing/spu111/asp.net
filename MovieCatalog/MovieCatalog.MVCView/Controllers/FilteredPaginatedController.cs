﻿using Microsoft.AspNetCore.Mvc;
using MovieCatalog.Application.Shared;
using MovieCatalog.Domain;
using MovieCatalog.Domain.Models;
using MovieCatalog.MVCView.ViewModels;
using MovieCatalog.MVCView.ViewModels.Shared;

namespace MovieCatalog.MVCView.Controllers;

public abstract class FilteredPaginatedController<TModel> : Controller where TModel : Model
{
    private readonly ICrudService<TModel> _crudService;
    
    protected FilteredPaginatedController(ICrudService<TModel> crudService)
    {
        _crudService = crudService;
    }
    
    public virtual IActionResult Index(
        string searchTerm = "", 
        string sorting = "",
        string sortColumn = "Id", 
        int page = 1, 
        int pageSize = 10)
    {
        var (sortBy, sortDirection) = PaginatedFilteredViewModel.GetSortColumnAndDirection(sorting, sortColumn);
        
        if (page <= 0)
            page = 1;
        
        return RedirectToAction("Filter", new
        {
            searchTerm,
            sortColumn = sortBy,
            sortDirection,
            page,
            pageSize
        });
    }
    
    public virtual async Task<IActionResult> Filter(
        string searchTerm = "",
        string sortColumn = "",
        string sortDirection = "",
        int page = 1,
        int pageSize = 10,
        CancellationToken cancellationToken = default)
    {
        var sortOrder = sortDirection.Equals("Descending") ? SortOrder.Desc : SortOrder.Asc;
        
        var dto = new FilterPaginationDto(searchTerm, page, pageSize, sortColumn, sortOrder);
        var categories = await _crudService.GetAllAsync(dto, cancellationToken);
        var viewModel = new CategoriesViewModel
        {
            Total = categories.Total,
            SortColumn = sortColumn,
            SortDirection = sortDirection,
            CurrentPage = page,
            PageSize = pageSize,
            SearchTerm = searchTerm
        };
        viewModel.AddModels(categories.Models);
        return View("Index", viewModel);
    }
}