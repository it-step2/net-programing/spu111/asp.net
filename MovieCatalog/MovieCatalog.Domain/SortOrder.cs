﻿namespace MovieCatalog.Domain;

public enum SortOrder
{
	Asc,
	Desc
}
